package com.ThomasBellier.Pokedex.config;

import com.ThomasBellier.Pokedex.models.Pokemon;
import com.ThomasBellier.Pokedex.repository.PokemonRepository;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

class CustomerItemWriter extends JpaItemWriter<Pokemon> {

    @Autowired
    private PokemonRepository pokemonRepository;


    @Override
    public void write(List<? extends Pokemon> pokemons) {

        pokemonRepository.saveAll(pokemons);
        System.out.println(pokemons);
    }
}
