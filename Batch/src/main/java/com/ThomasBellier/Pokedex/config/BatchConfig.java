package com.ThomasBellier.Pokedex.config;

import com.ThomasBellier.Pokedex.models.Pokemon;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration
@EnableBatchProcessing
@ActiveProfiles("application-default.properties")
public class BatchConfig extends DefaultBatchConfigurer {

    @Override
    public void setDataSource(DataSource dataSource) {
    }

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("jpaTrx")
    PlatformTransactionManager jpaTransactionManager;


    @Bean
    public Job readCSVFilesJob(Step step) {
        return jobBuilderFactory
                .get("readCSVFilesJob")
                .start(step)
                .build();
    }

    @Bean
    public Step step() throws Exception {
        return stepBuilderFactory.get("step")
                .transactionManager(jpaTransactionManager)
                .<Pokemon, Pokemon>chunk(10)
                .reader(reader())
                .writer(writer())
                .build();
    }

    @Bean
    @StepScope
    protected FlatFileItemReader<Pokemon> reader() throws Exception {
        DefaultLineMapper<Pokemon> defaultLineMapper = new DefaultLineMapper<>();
        defaultLineMapper.setLineTokenizer(new DelimitedLineTokenizer());
        defaultLineMapper.setFieldSetMapper(fieldSet -> {
            Pokemon result = new Pokemon();
            result.setNumber(fieldSet.readInt(0));
            result.setName(fieldSet.readString(1));
            result.setType1(fieldSet.readString(2));
            result.setType2(fieldSet.readString(3));
            result.setGeneration(fieldSet.readInt(11));
            result.setLegendary(fieldSet.readString(12));

            return result;
        });
        defaultLineMapper.afterPropertiesSet();
        FlatFileItemReader<Pokemon> reader = new FlatFileItemReader<>();
        reader.setLineMapper(defaultLineMapper);
        reader.setResource(new FileSystemResource("C:\\Users\\Thomas Bellier\\Desktop\\Pokedex\\src\\main\\resources\\data\\pokemon.csv"));
        reader.afterPropertiesSet();
        return reader;
    }

    @Bean
    public JpaItemWriter<Pokemon> writer() {
        JpaItemWriter<Pokemon> writer = new JpaItemWriter<>();
        writer.setEntityManagerFactory(entityManagerFactory);
        return writer;
    }

    @Bean
    @Qualifier("jpaTrx")
    public PlatformTransactionManager jpaTransactionManager() {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
