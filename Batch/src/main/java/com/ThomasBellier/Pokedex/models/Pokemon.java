package com.ThomasBellier.Pokedex.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name="pokemons")
public class Pokemon implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Basic
    @Column(name = "number")
    private int number;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "type1")
    private String type1;
    @Basic
    @Column(name = "type2")
    private String type2;
    @Basic
    @Column(name = "generation")
    private int generation;
    @Basic
    @Column(name = "legendary")
    private String legendary;
}
