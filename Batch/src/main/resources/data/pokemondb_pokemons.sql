CREATE DATABASE  IF NOT EXISTS `pokemondb`;
USE `pokemondb`;

--
-- Table structure for table `pokemons`
--

DROP TABLE IF EXISTS `pokemons`;

CREATE TABLE `pokemons` (
  `id` int NOT NULL AUTO_INCREMENT,
  `number` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `type1` varchar(45) NOT NULL,
  `type2` varchar(45) DEFAULT NULL,
  `generation` int NOT NULL,
  `legendary` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pokemon_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
