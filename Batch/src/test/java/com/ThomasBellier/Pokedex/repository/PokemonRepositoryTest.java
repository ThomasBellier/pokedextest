package com.ThomasBellier.Pokedex.repository;

import com.ThomasBellier.Pokedex.models.Pokemon;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@SpringBootTest
class PokemonRepositoryTest {

    @Autowired
    private PokemonRepository pokemonRepository;

    @Test
    void insertPokemonsIntoDatabase() {
        Pokemon pokemonToInsert = Pokemon.builder()
                .generation(1)
                .name("Bulbasaur")
                .number(1)
                .id(1L)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        Pokemon pokemonExpected = Pokemon.builder()
                .generation(1)
                .name("Bulbasaur")
                .number(1)
                .id(1L)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        Pokemon secondPokemonToInsert = Pokemon.builder()
                .generation(1)
                .name("Ivysaur")
                .number(2)
                .id(2L)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        Pokemon secondPokemonExpected = Pokemon.builder()
                .generation(1)
                .name("Ivysaur")
                .number(2)
                .id(2L)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        List<Pokemon> pokemons = pokemonRepository.saveAll(List.of(pokemonToInsert, secondPokemonToInsert));


        assertThat(pokemons).containsExactlyInAnyOrder(pokemonExpected, secondPokemonExpected);
    }
}

