package com.ThomasBellier.REST_API.controller;

import com.ThomasBellier.REST_API.models.Pokemon;
import com.ThomasBellier.REST_API.repository.PokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
class PokemonController {

    @Autowired
    PokemonRepository pokemonRepository;

    @GetMapping("/pokemons")
    public ResponseEntity<List<Pokemon>> getAll() {

        List<Pokemon> pokemons = pokemonRepository.findAll();

        return new ResponseEntity(pokemons, HttpStatus.OK);
    }

    @GetMapping("/pokemon")
    public ResponseEntity<Pokemon> getPokemonByName(@RequestParam(required = true) String name) {
        System.out.println(name);
        Pokemon pokemon = pokemonRepository.findByName(name);

        return new ResponseEntity(pokemon, HttpStatus.OK);
    }

}
