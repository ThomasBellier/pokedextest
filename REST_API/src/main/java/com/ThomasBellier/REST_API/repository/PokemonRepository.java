package com.ThomasBellier.REST_API.repository;

import com.ThomasBellier.REST_API.models.Pokemon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PokemonRepository extends JpaRepository<Pokemon, Long> {

    Pokemon findByName(String name);
}
