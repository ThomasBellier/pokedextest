package com.ThomasBellier.REST_API.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Entity
@Table(name="pokemons")
public class Pokemon {

    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "generation")
    private Integer generation;
    @Basic
    @Column(name = "legendary")
    private String legendary;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "number")
    private Integer number;
    @Basic
    @Column(name = "type1")
    private String type1;
    @Basic
    @Column(name = "type2")
    private String type2;
}
