package com.ThomasBellier.REST_API.controller;

import com.ThomasBellier.REST_API.models.Pokemon;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@SpringBootTest
@ActiveProfiles("application.properties")
class PokemonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllPokemons() throws Exception {

        Pokemon pokemonExpected = Pokemon.builder()
                .generation(1)
                .name("Bulbasaur")
                .number(1)
                .id(1)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        Pokemon secondPokemonExpected = Pokemon.builder()
                .generation(1)
                .name("Ivysaur")
                .number(2)
                .id(2)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        mockMvc.perform(get("/pokemons").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(pokemonExpected.getId())))
                .andExpect(jsonPath("$[0].name", is(pokemonExpected.getName())))
                .andExpect(jsonPath("$[0].generation", is(pokemonExpected.getGeneration())))
                .andExpect(jsonPath("$[0].type1", is(pokemonExpected.getType1())))
                .andExpect(jsonPath("$[0].type2", is(pokemonExpected.getType2())))
                .andExpect(jsonPath("$[0].legendary", is(pokemonExpected.getLegendary())))
                .andExpect(jsonPath("$[0].number", is(pokemonExpected.getNumber())))
                .andExpect(jsonPath("$[1].id", is(secondPokemonExpected.getId())))
                .andExpect(jsonPath("$[1].name", is(secondPokemonExpected.getName())))
                .andExpect(jsonPath("$[1].generation", is(secondPokemonExpected.getGeneration())))
                .andExpect(jsonPath("$[1].type1", is(secondPokemonExpected.getType1())))
                .andExpect(jsonPath("$[1].type2", is(secondPokemonExpected.getType2())))
                .andExpect(jsonPath("$[1].legendary", is(secondPokemonExpected.getLegendary())))
                .andExpect(jsonPath("$[1].number", is(secondPokemonExpected.getNumber())));
    }

    @Test
    void getPokemonByName() throws Exception {
        Pokemon pokemonExpected = Pokemon.builder()
                .generation(1)
                .name("Ivysaur")
                .number(2)
                .id(2)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        mockMvc.perform(get("/pokemon?name=Ivysaur").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(pokemonExpected.getId())))
                .andExpect(jsonPath("$.name", is(pokemonExpected.getName())))
                .andExpect(jsonPath("$.generation", is(pokemonExpected.getGeneration())))
                .andExpect(jsonPath("$.type1", is(pokemonExpected.getType1())))
                .andExpect(jsonPath("$.type2", is(pokemonExpected.getType2())))
                .andExpect(jsonPath("$.legendary", is(pokemonExpected.getLegendary())))
                .andExpect(jsonPath("$.number", is(pokemonExpected.getNumber())));
    }
}
