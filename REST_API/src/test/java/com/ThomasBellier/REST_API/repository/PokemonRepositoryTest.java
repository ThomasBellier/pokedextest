package com.ThomasBellier.REST_API.repository;

import com.ThomasBellier.REST_API.models.Pokemon;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("application.properties")
@SpringBootTest
class PokemonRepositoryTest {

    @Autowired
    private PokemonRepository pokemonRepository;

    @Test
    void getAllPokemons() {
        Pokemon pokemonExpected = Pokemon.builder()
                .generation(1)
                .name("Bulbasaur")
                .number(1)
                .id(1)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        Pokemon secondPokemonExpected = Pokemon.builder()
                .generation(1)
                .name("Ivysaur")
                .number(2)
                .id(2)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        List<Pokemon> pokemons = pokemonRepository.findAll();

        assertThat(pokemons).contains(pokemonExpected, secondPokemonExpected);

    }

    @Test
    void getPokemonByName() {
        Pokemon pokemonExpected = Pokemon.builder()
                .generation(1)
                .name("Bulbasaur")
                .number(1)
                .id(1)
                .legendary("False")
                .type1("Grass")
                .type2("Poison")
                .build();

        Pokemon pokemonFound = pokemonRepository.findByName(pokemonExpected.getName());

        assertThat(pokemonFound).isEqualTo(pokemonExpected);
    }

}
